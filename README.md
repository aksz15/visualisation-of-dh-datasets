# Methods and Tools for visualising Digital Humanities data sets

### Andreas Kuczera

You can find the website at https://aksz15.git-pages.thm.de/visualisation-of-dh-datasets/index.html

![Visualisation tools used in the workshop](tools.png "Visualise all the things")

The website, the data and the visualisations are released under [CC-BY 4.0](https://creativecommons.org/licenses/by/4.0/)

This page is a fork of https://github.com/digicademy/dhd2016-workshop
